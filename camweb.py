import cv2
import numpy as np

from imutils.video.pivideostream import PiVideoStream
import time

from tornado import web, ioloop

import config


# Initialize camera

vs = PiVideoStream(resolution=(1280, 720), framerate=60).start()
vs.camera.start_preview()

def update_camera_settings():
    vs.camera.brightness = config.settings["brightness"]
    vs.camera.contrast = config.settings["contrast"]
    vs.camera.shutter_speed = config.settings["shutter_speed"]
    vs.camera.awb_mode = config.settings["awb_mode"]
    vs.camera.awb_gains = (config.settings["awb_red"], config.settings["awb_blue"])

update_camera_settings()

time.sleep(0.5)



def send_jpg(handler, image):
    ret, buf = cv2.imencode(".jpg", image)
    handler.set_header('Content-type', 'image/jpg')
    handler.set_header('Content-length', len(buf))
    handler.write(buf.tostring())

def send_png(handler, image):
    ret, buf = cv2.imencode(".png", image)
    handler.set_header('Content-type', 'image/png')
    handler.set_header('Content-length', len(buf))
    handler.write(buf.tostring())


class RawHandler(web.RequestHandler):
    def get(self):
        image = vs.read()
        send_jpg(self, image)

class ThresholdedHandler(web.RequestHandler):
    def get(self):
        image = vs.read()
        threshold_value = config.settings["threshold_value"]
        _, thresholded = cv2.threshold(image[:,:,0], threshold_value, 255, 0)
        send_png(self, thresholded)

app = web.Application([
    (r'/raw', RawHandler),
    (r'/thresholded', ThresholdedHandler)
])

app.listen(80)
ioloop.IOLoop.instance().start()
